<?php

Route::get('/', function () {
    return view('welcome');
});






Route::get('/FormAdmin','AdminController@index');

Route::get('/infouser','UsersController@index');
Route::get('/infouser/hapus/{id}','UsersController@hapus');

Route::get('/grandprize','AdminController@grandPrize');

Route::get ('/berita','BeritaController@blog');
Route::get ('/MenuBerita/tambah','BeritaController@tambah');
Route::post('/MenuBerita/insert','BeritaController@insert');
Route::get('/berita/hapus/{id}','BeritaController@hapus');

/*
| PRODUK
*/
Route::get ('/produk','ProdukController@produk');
Route::get ('/menuproduk/tambah','ProdukController@tambah');
Route::post('/menuproduk/insert','ProdukController@insert');
Route::get('/produk/edit/{id}','ProdukController@edit');
Route::patch('/produk/update/{id}','ProdukController@update');
Route::get('/produk/hapus/{id}','ProdukController@hapus');
Route::resource('produk', 'ProdukController');

/*
---------------------------------------------------------------------------------------------------------------------------------
																*/

/*	
|-	TRANSAKSI
*/
Route::get('/transaksi','AdminController@transaksi');


/*
---------------------------------------------------------------------------------------------------------------------------------
																*/
/*
|-	Banner
*/
Route::get('/banner','AdminController@banner');
Route::get('/index', 'BannerController@index');
Route::get ('MenuBanner/create', 'BannerController@tambah');
Route::post('MenuBanner/insert', 'BannerController@insert');
Route::get('/banner/hapus/{id}','BannerController@hapus');
Route::resource('banner', 'BannerController');

																/*
---------------------------------------------------------------------------------------------------------------------------------
																*/
/*
|-	KategoriProduk
*/
Route::get('/kategoriproduk','AdminController@kategoriproduk');
Route::get ('MenuKategoriProduk/tambah','KategoriController@tambah');
Route::get('/index', 'KategoriController@index');
Route::resource('kategoriproduk' , 'KategoriController');
Route::get('kategoriproduk/hapus/{id}','KategoriController@hapus');
/*
---------------------------------------------------------------------------------------------------------------------------------
																*/


/*
|--------------------------------------------------------------------------
| Route User
|--------------------------------------------------------------------------
*/
Route::get('/FormUser','UserController@index')->name('users');	
Route::get('/about','UserController@about');
Route::get('/contact','UserController@contact');
Route::get('/whislist','UserController@whislist');
Route::get('/cart','UserController@cart')->middleware('auth');


/*
|--------------------------------------------------------------------------
| Raja Ongkir
|--------------------------------------------------------------------------
*/
Route::get('/checkout','PageController@getprovince');
Route::get('/checkout','PageController@getcity');
Route::get('/checkout','PageController@checkshipping');
Route::post('checkout', 'PageController@processShipping');
Route::get('/checkout','PageController@index');
Route::get('/contentongkir','PageController@indexTo');

Route::post('/bayar', 'PageController@bayar');
Route::get('/show', 'PageController@show');









Route::get('/log','UserController@log');
Route::get('/reg','UserController@reg');

/*
|--------------------------------------------------------------------------
| Shop
|--------------------------------------------------------------------------
*/
Route::get('/shop','ShopController@index');
Route::get('/shop','ShopController@shop');
Route::post('/shop/{nama_kategori}','ShopController@katShop');
Route::get('/shop/{nama_kategori}','ShopController@katShop');
Route::resource('shop', 'ShopController');
Route::get('MenuShop/produksingel','ShopController@produksingel');
Route::get('MenuShop/produksingel/{id}','ShopController@detSingel');
/*
|--------------------------------------------------------------------------
| Blog
|--------------------------------------------------------------------------
*/
Route::get('/blog','BlogController@index');
Route::get('/blog','BlogController@content');
Route::get('/readmore','UserController@readmore');
Route::get('MenuBlog/isireadmore/{id}','BlogController@readmore');
/*
|--------------------------------------------------------------------------
| Login & Register
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('FormUser/user', 'HomeController@index')->name('user');

Route::post('/add-to-cart/{id}', 'CartController@cart');
Route::get('/cart/hapus/{id}','CartController@hapus');

