 <div class="card-body">
  <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">

  @foreach($banner->take(3) as $row)
    <div class="carousel-item d-flex justify-content-center @if($loop->first) active @endif">
      <img src="{{ url('uploadbanner') }}/{{ $row->file_gambar }}" alt="{{$row->judul}}" 
      style="width: 1000px; height: 400px;" >
    </div>
  @endforeach
  </div>

    <a href="#carousel" role="button" data-slide="prev" class="carousel-control-prev">
      <span class="carousel-control-prev-icon" aria-hidden="ture"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a href="#carousel" role="button" data-slide="next" class="carousel-control-next">
      <span class="carousel-control-next-icon" aria-hidden="ture"></span>
      <span class="sr-only">Next</span>
    </a>
    

  </div>
  


</div>