<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Produksi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('user/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('user/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('user/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/style.css')}}">
  </head>
  <body class="goto-here">
		<!--headerhed-->
        @include ('FormUser/MenuShop/headerhed')
    <!--End Header-->
    <!--navbar-->
        @include ('FormUser/navbar')
    <!--End navbar-->
    <!--BackHome-->
      @include ('FormUser/MenuShop/backhome')

    <!--End BackHome-->
    <!--contentproduksi-->
     <section class="ftco-section">
      <div class="container">
        <div class="row">
        @foreach($produk as $pro)
          <div class="col-lg-6 mb-5 ftco-animate">
          <center><img style="width: 300px;height: 400px;" src="{{url('/uploadgambar',$pro->file)}}" class="img-fluid"></center>
          </div>
          <div class="col-lg-6 product-details pl-md-5 ftco-animate">
             <h3><a>{{ $pro->nama_produk }}</a></h3>
            <p class="price"><span>Rp.{{$pro->harga}}</span></p>
            <p>{{$pro->deskripsi}}</p>    
              <div class="w-100"></div>
              <div class="input-group col-md-6 d-flex mb-3">
                <span class="input-group-btn mr-2">
                    <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                     <i class="ion-ios-remove"></i>
                    </button>
                  </span>
                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                <span class="input-group-btn ml-2">
                    <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                       <i class="ion-ios-add"></i>
                   </button>
                </span>
            </div>
            <p><a href="{{url('cart')}}" class="btn btn-black py-3 px-5">Add to Cart</a></p>
            @endforeach
          </div>
        </div>
    </section>
    <!--End contentproduksi-->
  @include ('FormUser/MenuShop/ket')
    <!--Produklain-->
   <section class="ftco-section">
        <div class="container">
            <div class="row" >
                          @foreach ($produks as $produk)
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="/MenuShop/produksingel/{{$produk->id}}" class="img-prod">
                        <img style="width: 300px; height: 200px;" class="img-fluid" 
                        src="{{url('/uploadgambar/' .$produk->file  )}}">
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center" style="height: 200px;" >
                            <h3><a href="/MenuShop/produksingel/{{$produk->id}}">{{ $produk->nama_produk }}</a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price">Rp.{{ $produk->harga }}</p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-cart"></i></span>
                                    </a>
                                   
                                </div>
                            </div> 
                        </div>

                    </div>

 
                </div>
                      @endforeach
                </div>
                </div>
                </section>

    <!--End Produklain-->
    <!--footer-->
      @include ('FormUser/MenuProduksi/footer')
    <!--End footer-->
    <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="{{asset('user/js/jquery.min.js')}}"></script>
  <script src="{{asset('user/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('user/js/popper.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('user/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('user/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('user/js/aos.js')}}"></script>
  <script src="{{asset('user/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('user/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('user/js/google-map.js')}}"></script>
  <script src="{{asset('user/js/main.js')}}"></script>

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>