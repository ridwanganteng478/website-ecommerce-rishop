 <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 mb-5 text-center">
                    <ul class="product-category">
                        <li><a href="shop" class="">All</a></li>
                        <li><a href="shopkemeja" class="">Kemeja</a></li>
                        <li><a href="shopcelana" class="active">Celana</a></li>
                        <li><a href="shopjaket" class="">Jeket</a></li>
                        <li><a href="shopkaos" class="">Kaos</a></li>
                    </ul>
                </div>
            </div>
             @foreach ($produk as $produk)
            <div class="row">
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <img class="img-fluid" src="{{url('/uploadgambar/celana/' .$produk->file)}}">
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3>{{$produk->nama_produk}}</h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price">{{$produk->harga}}</p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a href="#" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="ion-ios-menu"></i></span>
                                    </a>
                                    <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-cart"></i></span>
                                    </a>
                                    <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                        <span><i class="ion-ios-heart"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                @endforeach
            
    </section>