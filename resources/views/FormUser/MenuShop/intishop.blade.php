    <center><a class="navbar-brand" style="font-size: 30px;">SHOP</a></center>
   <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 mb-5 text-center">
                    <ul class="product-category">
                        <li><a href="/shop">All</a></li>
                        @foreach($filter as $fil)
                        <li><a href="{{url('shop',$fil->nama_kategori)}}">{{$fil->nama_kategori}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row" >
                          @foreach ($produk as $produk)
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="/MenuShop/produksingel/{{$produk->id}}" class="img-prod">
                        <img style="width: 300px; height: 200px;" class="img-fluid" 
                        src="{{url('/uploadgambar/' .$produk->file  )}}">
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center" style="height: 200px;" >
                            <h3><a href="/MenuShop/produksingel/{{$produk->id}}">{{ $produk->nama_produk }}</a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price">Rp.{{ $produk->harga }}</p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                <form action="{{ url('/add-to-cart/'.$produk->id) }}" method="post">
                                @csrf
                                    <button type="submit" class="buy-now d-flex justify-content-center align-items-center mx-1"><i class="ion-ios-cart"></i></button>
                                </form>
                                </div>
                            </div> 
                        </div>

                    </div>

 
                </div>
                      @endforeach
                </div>
                </div>
                </section>
