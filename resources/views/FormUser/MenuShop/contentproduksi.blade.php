    <section class="ftco-section">
      <div class="container">
        <div class="row">
        @foreach($produk as $pro)
          <div class="col-lg-6 mb-5 ftco-animate">
            <a  class="image-popup"><img src="{{url('/uploadgambar',$pro->file)}}" class="img-fluid" 
           ></a>
          </div>
          <div class="col-lg-6 product-details pl-md-5 ftco-animate">
             <h3><a>{{ $pro->nama_produk }}</a></h3>
            <p class="price"><span>Rp.{{$pro->harga}}</span></p>
            <p>{{$pro->deskripsi}}</p>
           
              <div class="w-100"></div>
              <div class="input-group col-md-6 d-flex mb-3">
                <span class="input-group-btn mr-2">
                    <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                     <i class="ion-ios-remove"></i>
                    </button>
                  </span>
                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                <span class="input-group-btn ml-2">
                    <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                       <i class="ion-ios-add"></i>
                   </button>
                </span>
            </div>
            <p>
            <form action="{{ url('/add-to-cart'. $pro->id) }}" method="post">
              <button type="submit" class="btn btn-black py-3 px-5">Add to Cart</button>
            </form>
            @endforeach
          </div>
        </div>
    </section>