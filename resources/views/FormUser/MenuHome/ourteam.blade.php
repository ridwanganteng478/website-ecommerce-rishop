    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Our Team</h2>
            <p>Web dibuat agar dapat mempermudak proses transaksi atau pembelian online tanpa harus datang ke TKP</p>
          </div>
        </div>
              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="user-img mb-5" style="background-image: url(user/images/bihis.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text text-center">
                    <p class="name">Ridwan Mutamasiqin</p>
                    <span class="position">(Web Developer)</span>
                  </div>
                </div>
              </div>
              
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>