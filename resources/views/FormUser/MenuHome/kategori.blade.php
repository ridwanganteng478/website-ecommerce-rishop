		<section class="ftco-section ftco-category ftco-no-pt">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6 order-md-last align-items-stretch d-flex">
								<div class="category-wrap-2 ftco-animate img align-self-stretch d-flex">
									<div class="text text-center">
										<h2>Toko</h2>
										
										<p><a href="{{url('shop')}}" class="btn btn-primary">Beli Sekarang</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image: url(user/images/kameja2.jpg);">
									<div class="text px-3 py-1">
										<h2 class="mb-0" >Kemeja</h2>
									</div>
								</div>
								<div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image: url(user/images/kaos.jpg);">
									<div class="text px-3 py-1">
										<h2 class="mb-0" >Kaos</h2>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image: url(user/images/celana.jpg);">
							<div class="text px-3 py-1">
								<h2 class="mb-0">Celana</h2>
							</div>		
						</div>
						<div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image: url(user/images/jaket2.jpg);">
							<div class="text px-3 py-1">
								<h2 class="mb-0">Jaket</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>