<section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
						<form action="#" class="billing-form">
							<center><h3 class="mb-4 billing-heading">Detai Pembayaran</h3></center>
	          	
	          <div class="row align-items-end">
	          		
                 <div class="w-100"></div>
                
					 
					<div class="col-xl-5">


	          <div class="row mt-8 pt-6">

	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4 d-flex align-item-center" >Total Bayar</h3>
	          			@foreach ($produk as $produk)
	          			<p class="d-flex">
		    						<span>{{$produk->nama_produk}}</span>
		    						<span>Rp . {{$produk->harga_produk}}</span>
		    					</p>
		    						@endforeach
		    					
		    					<hr>
		    					<p class="d-flex total-price">
		    						<span>Total</span>
		    						<span>rp . {{ $subtotal }} </span>
		    					</p>
								</div>
								
	          	</div>
	         
						</div>
									<center><p><a href="/contentongkir"class="btn btn-primary py-3 px-4">Bayar</a></p></center>
						</div>
						</div></form></div></div></div></section><
					
	      
