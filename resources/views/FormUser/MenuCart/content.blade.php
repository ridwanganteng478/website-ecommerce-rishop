    <center><a class="navbar-brand" style="font-size: 30px;">CART</a></center>
    <section class="ftco-section ftco-cart">
			<div class="container">
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>Opsi</th>
						        <th>Gambar Produk</th>
						        <th>Nama Produk</th>
						        <th>Harga</th>
						        <th>Stoky</th>
						        <th>Total</th>
						      </tr>
						    </thead>
						    <tbody> 
						    @foreach ($cart as $row)
						      <tr class="text-center">
						        <td class="product-remove"><a href="{{url('cart/hapus', $row->id) }}"><span class="ion-ios-close"></span></a></td>
						        
						        <td class="image-prod"><img src="{{url('/uploadgambar/'. $row->gambar_produk)}}" width="100" height="100"></td>
						        
						        <td class="product-name">
						        	<h3>{{$row->nama_produk}}</h3>
						        
						        </td>
						        
						        <td class="price">{{$row->harga_produk}}</td>
						        
						        <td class="quantity">
						        	<div class="input-group mb-3">
					             	<input type="text" name="quantity" class="quantity form-control input-number" value="1" min="1" max="100">
					          	</div>
					          </td>
						        
						        <td class="total">-------</td>
						      </tr><!-- END TR-->
						      @endforeach
						      </tbody>
						      </table>

						      
    			<center><p><a href="{{url('checkout')}}" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p></center>
    			</div>
    		</div>
			</div>
		</section>