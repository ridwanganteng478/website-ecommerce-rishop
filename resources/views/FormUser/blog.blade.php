<!DOCTYPE html>
<html lang="en">
  <head>
    <title>blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('user/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('user/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('user/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/style.css')}}">
  </head>
   <body class="goto-here">

    <!--Headerhed-->
      @include ('FormUser/MenuBlog/headerhed')
    <!--Headerhed-->

    <!-- Navbar -->
      @include ('FormUser/navbar')
    <!-- END nav -->

    <!--backhome-->
        @include ('FormUser/MenuBlog/backhome')
    <!--backhome-->

    <!--ContenBlog-->
        <center><a class="navbar-brand" style="font-size: 30px;">BLOG</a></center>
      <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
      <div class="row">
        @foreach($berita as $ber)
        <div class="col-md-12 d-flex ftco-animate">
                <div class="blog-entry align-self-stretch d-md-flex">
                 <img src="{{ url('uploadberita') }}/{{ $ber->file}}" style="width: 200px; height: 200px;" >
                  <div class="text d-block pl-md-4">
                    <h3 class="heading">{{$ber->judul}}</h3>
                    <p>{{$ber->deskripsi}}</p>
                    <p><a href="/MenuBlog/isireadmore/{{$ber->id}}" class="btn btn-primary py-2 px-3">Read more</a></p>
              </div>
             </div>
            </div>
             @endforeach
               
        </div>
       </div>
      </div>
     </div>
</section>
    <!--End ContenBlog-->

    <!--Recent-->
    <!--END Recent-->

    <!--footer-->
      @include ('FormUser/MenuBlog/footer')
    <!--AND footer-->
    <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <script src="{{asset('user/js/jquery.min.js')}}"></script>
  <script src="{{asset('user/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('user/js/popper.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('user/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('user/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('user/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('user/js/aos.js')}}"></script>
  <script src="{{asset('user/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('user/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('user/js/google-map.js')}}"></script>
  <script src="{{asset('user/js/main.js')}}"></script>
    
  </body>
</html>