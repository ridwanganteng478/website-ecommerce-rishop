<!DOCTYPE html>
<html>
<head>
	<title>GrandPrize</title>
	<link rel="shortcut icon"  href="img/jh.png">
</head>
	<style type="text/css">
	body{
  letter-spacing: 9px;
  font-size: 60px;
  text-align:center;
  line-height:100vh;
  font-family: Courier;

}

html{
 
  color: #fff;
}

button {
    background: transparent;
    color: #fff;
    border: 1px solid;
    padding: 10px 20px;
    font-size: 20px;
    border-radius: 2px;
    cursor: pointer;
}
button:hover {
    background: #fff;
    color:#000;
    transition: all 0.5s ease-in-out;
}
</style>

<body background="img/beg.jpg" style="background-size: cover;">
			<button onClick="start()">Start</button>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

var names = [@foreach($grand as $grd) " {{ $grd->nomor }}  {{ $grd->nama }}", @endforeach];
count = names.length;

function start() {
  selected = names[Math.random() * count | 0].toUpperCase();
  covered = selected.replace(/[^\s]/g, '_');
  document.body.innerHTML = covered;
  timer = setInterval(decode, 50);
}

function decode() {
  newtext = covered.split('').map(changeLetter()).join('');
  document.body.innerHTML = newtext;

  if (selected == covered) {
    clearTimeout(timer);
    winnerRevealed();
    return false;
  }

  covered = newtext;
}

function changeLetter() {
  replacements = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
  replacementsLen = replacements.length;
  return function (letter, index, err) {
    return selected[index] == letter ? letter : replacements[Math.random() * replacementsLen | 0];
  };
}
</script>

</html>