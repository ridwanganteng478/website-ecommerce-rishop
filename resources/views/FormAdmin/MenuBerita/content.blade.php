<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Admin</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
   <body>

 
<div class="row">
  <div class="col-md-6">
   
  </div>
</div>


<div class="card">
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                 <div class="card-footer clearfix">
                <a href="MenuBerita/tambah" class="btn btn-sm btn-info float-left"> + Berita</a>
             </div>
                  <table class="table m-0">
                    <thead>
                    <tr>

                       <th style="text-align: center;">File</th>
                       <th style="text-align: center;">Judul</th>
                       <th style="text-align: center;">Deskripsi</th>
                       <th style="text-align: center;">Readmore</th>
                       <th style="text-align: center;">Opsi</th>
                    </tr>
                    </thead>
                     @foreach($berita as $new)
                        <tr>
                        <td style="text-align: center; "><img src="{{ url('uploadberita') }}/{{ $new->file }}" class="img-responsive" style="width: 200px; height: 200px;"></td>
                         <td  style="text-align: center;">{{ $new->judul }}</td>
                        <td  style="text-align: center;">{{ $new->deskripsi }}</td>
                        <td  style="text-align: center;">{{ $new->readmore }}</td>
                       <td>   
                       <center  >
                       <button style="margin-top: 10px;" type="button" class="btn btn-outline-danger"> 
                       <a href="{{url('berita/hapus', $new->id) }}">
                       Delete</a></button>  
                       </center>
                        </td>
                          </tr>
                    @endforeach

                  </table>
                <center>{{ $berita->links() }}</center>
                </div>

                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{asset('admin/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('admin/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-mapael/maps/world_countries.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{asset('admindist/js/pages/dashboard2.js')}}"></script>
</body>
</html>
