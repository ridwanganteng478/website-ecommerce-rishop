<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a class="d-block">Ridwan</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{('FormAdmin')}}" class="nav-link active">
             <img src="admin/dist/img/dasboard.png">
              <p>
                Dashboard
              </p>
            </a>
          <li class="nav-item has-treeview">
            <a href="{{('banner')}}" class="nav-link">
            <img src="admin/dist/img/banner.png">
              <p>
                Banner
              </p>
            </a>
          </li>
       <li class="nav-item has-treeview">
            <a href="{{('infouser')}}" class="nav-link">
            <img src="admin/dist/img/user2.png">
              <p>
                Info User
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{('kategoriproduk')}}" class="nav-link">
            <img src="admin/dist/img/category.png">
              <p>
                Kategori Produk
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{('produk')}}" class="nav-link">
            <img src="admin/dist/img/product.png">
              <p>
                Produk
              </p>
            </a>
          </li>
             <li class="nav-item has-treeview">
            <a href="{{('transaksi')}}" class="nav-link">
            <img src="admin/dist/img/transaksi.png">
              <p>
                Transaksi
              </p>
            </a>
          </li>

          <li class="nav-header">TENTANG KAMI</li>
          
          <li class="nav-item has-treeview">
             <a href="{{('berita')}}" class="nav-link">
              <img src="admin/dist/img/berita.png">
              <p>
                Berita
              </p>
            </a>
           
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
 </ul>
</li>
         
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">DASHBOARD</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{('FormAdmin')}}">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
 
    <!-- /.content-header -->