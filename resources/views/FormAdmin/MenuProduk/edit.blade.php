
<!--........................................................................................................................-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Admin</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
@include ('FormAdmin/MenuProduk/MenuEdit/sidebar')


<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> EDIT PRODUK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/produk">Kembali</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                @foreach($produk as $p)
            <form action="{{ url('/produk/update',@$p->id) }}" method="post" enctype="multipart/form-data">
            @csrf
                  @if(!empty($produk))
                  @method('PATCH')
                  @endif
                    @if(count($errors) > 0)
            <div class="row">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
            </div>
        @endif
                <input type="hidden" name="id" value="{{ $p->id }}"> <br/>
                <div class="card-body">
                  <div class="form-group">
                    <label for="">Nama Produk</label>
                    <input style="width: 200px; height: 50px; text-align: center;" type="text" class="form-control"
                     id="namaproduk" placeholder="Nama Produk" required="required" value="{{ $p->nama_produk }}" name="nama_produk">
                    </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Kategori Produk</label>
                    <p>
                    <select name="jenis">
                    @foreach($nama_kategori as $row)
                    <option  value="{{$row->nama_kategori}}">{{old('jenis', $row->nama_kategori) }}</option> 
                    @endforeach
                </select>
                </p>
                </div>
                  <div class="form-group">
                    <label for="file">File input</label>
                    <div class="input-group" >
                        <input type="file" id="file" name="file">
                      
                      </div>
                    </div>
                  <div class="form-group">
                    <label for="z">Deskripsi</label>
                    <p>
                    <textarea required="required" name="deskripsi" style="width:400px; height: 100px;">{{ $p->deskripsi }}</textarea> 
                    </p>
                  </div>
                  <div class="form-group">
                    <label for="harga">harga</label>
                    <input style="width: 200px; height: 50px; text-align: center;" type="number" required="required" name="harga"value="{{ $p->harga }}">
                  </div>

                  
                <!-- /.card-body -->

                <div class="card-footer">
                 <input type="submit" value="Simpan Data">

                </div>
                </div>
              </form>
               @endforeach
            </div>
            </div>
          
            <!-- /.card -->
            <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{asset('admin/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('admin/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-mapael/maps/world_countries.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{asset('admindist/js/pages/dashboard2.js')}}"></script>
