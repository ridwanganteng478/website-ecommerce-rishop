<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = ['id','nama_user', 'nama_produk','gambar_produk','harga_produk','stok'];
}
