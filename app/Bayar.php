<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bayar extends Model
{
     protected $table = 'bayar';
     protected $fillable = ['city','province','no_hp','nama_produk','harga','ongkir','total'];
}
