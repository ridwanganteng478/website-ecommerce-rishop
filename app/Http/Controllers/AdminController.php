<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
		public function index(){

				return view('FormAdmin/admin');
		}
		public function banner(){

				return view('FormAdmin/banner');
		}
		public function infouser(){

				return view('FormAdmin/infouser');
		}
		public function kategoriproduk(){

				return view('FormAdmin/kategoriproduk');
		}
		public function transaksi(){

				return view('FormAdmin/transaksi');
		}


		public function grandPrize(){

				$data['grand'] = \DB::table('undian')->get();
				return view('FormAdmin/grand_prize', $data);

		}
}
