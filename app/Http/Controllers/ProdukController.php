<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Produk;
use App\Http\Requests;
use App\Kategori;

class ProdukController extends Controller
{
    public function index(){

		// mengambil data dari table pegawai
    	$data['produk'] = \DB::table('produk')->paginate(3);
        return view('FormAdmin/produk',$data);
				
		}

	public function tambah(){

		$nama_kategori = Kategori::all();
		return view('FormAdmin/MenuProduk/tambah', compact('nama_kategori'));
	}

   
	public function insert(Request $request){	
	// insert data ke table pegawai
	 $this->validate($request, [
            'nama_produk' => 'required',
            'jenis' => 'required',
            'file' => 'required|image',
            'deskripsi' => 'required',
            'harga' => 'required'

            ]);
        $produk = $request->file('file');
        $namaFile = $produk->getClientOriginalName();
        $request->file('file')->move('uploadgambar', $namaFile);
        $do = new Produk($request->all());
        $do->file = $namaFile;
        $do->save();
    
	// alihkan halaman ke halaman pegawai
		return redirect('/produk');

	}

	public function edit($id){

	$produk = DB::table('produk')->where('id',$id)->get();
	$data['nama_kategori'] = DB::table('kategori_produk')->get();
	return view('FormAdmin/MenuProduk/edit',['produk'=> $produk], $data);
	
		}

	public function update(Request $request, $id)
{
	$rule= [
            'nama_produk' => 'required',
            'jenis' => 'required',
            'file' => 'required|image',
            'deskripsi' => 'required',
            'harga' => 'required'

            ];

    $this->validate($request, $rule);
	$input = $request->all();

	$produk = \App\Produk::find($id);
	$produk ->nama_produk = $input['nama_produk'];
	$produk ->jenis = $input['jenis'];
	$produk ->deskripsi = $input['deskripsi'];
	$produk ->harga = $input['harga'];
	if($request->file('file')==""){
		$produk->file = $produk->file;
	}
	 else {

	 	$file = $request->file('file');
	 	$fileName = $file->getClientOriginalName();
	 	$request->file('file')->move('uploadgambar',$fileName);
	 	$produk->file = $fileName;
	 }

	 $produk->update();

	return redirect('/produk');
}

	public function hapus($id)
		{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('produk')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/produk');
}
}
