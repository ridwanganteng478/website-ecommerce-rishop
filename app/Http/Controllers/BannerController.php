<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gambar;
use App\Http\Requests;

class BannerController extends Controller
{
    public function index()
    {
        $gambar = Gambar::all();
        return view('FormAdmin/banner', compact('gambar'));
    }

    public function tambah()
    {
        return view('FormAdmin/MenuBanner/create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'file_gambar' => 'required|image',
            ]);
        $gambar = $request->file('file_gambar');
        $namaFile = $gambar->getClientOriginalName();
        $request->file('file_gambar')->move('uploadbanner', $namaFile);
        $do = new Gambar($request->all());
        $do->file_gambar = $namaFile;
        $do->save();
        return redirect('/banner');
    }
    public function hapus($id)
        {
    // menghapus data pegawai berdasarkan id yang dipilih
    DB::table('gambar')->where('id',$id)->delete();
        
    // alihkan halaman ke halaman pegawai
    return redirect('/banner');
}
}