<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Users;

class UsersController extends Controller
{
    public function index(){

		// mengambil data dari table pegawai
    	$users = Users::paginate(5);
        return view('FormAdmin/infouser', compact('users'));
    }
    public function hapus($id)
		{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('users')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/infouser');
}
}
