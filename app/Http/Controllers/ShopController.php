<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Gambar;
use App\Kategori;
use App\Cart;


class ShopController extends Controller
{

    public function index()
  {
    $data['banner'] = \DB::table('gambar')->get();
		$data['produk'] = \DB::table('produk')->get();
    $data2 ['filter'] = \DB::table('kategori_produk')->get();
    return view('FormUser/shop', $data,$data2);

  }
  	public function katShop(Request $request)
  {
    $data['banner'] = \DB::table('gambar')->get();
		$filter = $request->nama_kategori;
		$data['produk'] = \DB::table('produk')->where('jenis', '=' , $filter)->get();
		$data2 ['filter'] = \DB::table('kategori_produk')->get();
		return view('FormUser/shop', $data,$data2);
  }
  	public function produksingel(){
     
      $data['produk'] = \DB::table('produk')->get();
      return view('FormUser/MenuShop/produksingel', $data);

    }
    public function detSingel($id){
    
  $data['banner'] = \DB::table('gambar')->get();
  $produk['produk'] = \DB::table('produk')->where('id',$id)->get();
  $data['produks'] = \DB::table('produk')->get();
  return view('FormUser/MenuShop/produksingel',$produk,$data);
  
    }
   

}
