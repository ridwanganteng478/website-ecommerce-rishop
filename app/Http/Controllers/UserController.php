<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Gambar;
use App\Cart;

class UserController extends Controller
{
		public function index(){

		
		$gambar = Gambar::all();
        return view('FormUser/user', compact('gambar'));
 		}
 		  
 	     public function shop()
	 	{
	 	
		$produk = Produk::all();
		return view('FormUser/shop', compact('produk'));		
 		 }
 		  public function blog()
	 	 {
	 	
		return view('FormUser/blog');		
 		 }
		

		public function about(){
			
			return view('FormUser/about');
		}
		public function contact(){
			
			return view('FormUser/contact');
		}
		
		public function readmore(){
			
			return view('FormUser/readmore');
		}
		
		public function whislist(){
			
			return view('FormUser/whislist');
		}
		public function cart(){
			$data['banner'] = \DB::table('gambar')->get();
			$data['cart'] = Cart::where('nama_user', auth()->user()->name)->get();
			return view('FormUser/cart', $data);
		}
		
		public function kemeja(){
			return view('FormUser/MenuShop/ShopKemeja/shopkemeja');
		}
		public function kaos(){
			return view('FormUser/MenuShop/ShopKaos/shopkaos');
		}
		public function jaket(){
			return view('FormUser/MenuShop/ShopJaket/shopjaket');
		}
		public function celana(){
		$produk = Produk::all();
        return view('FormUser/MenuShop/ShopCelana/shopcelana', compact('produk'));
		}
		public function log(){
			
			return view('FormUser/login');
		}
		public function reg(){
			return view('FormUser/register');
		}



}
