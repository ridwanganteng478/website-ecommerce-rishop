<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

use Illuminate\Support\Facades\DB;

use App\Http\Requests;


class BeritaController extends Controller
{
    	public function blog()
    	{
		$data['berita'] = \DB::table('berita')->paginate(3);
        return view('FormAdmin/berita',$data);
		}

	public function tambah()
	{

	return view('FormAdmin/MenuBerita/tambah');
	}

   
	public function insert(Request $request){	
	// insert data ke table pegawai
	 $this->validate($request, [
            'file' => 'required|image',
            'judul' => 'required',
            'deskripsi' => 'required',
            'readmore' => 'required'

            ]);
        $berita = $request->file('file');
        $namaFile = $berita->getClientOriginalName();
        $request->file('file')->move('uploadberita', $namaFile);
        $do = new Berita($request->all());
        $do->file = $namaFile;
        $do->save();
    
	// alihkan halaman ke halaman pegawai
		return redirect('/berita');

	}

	public function hapus($id)
		{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('berita')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/berita');
		}
}
