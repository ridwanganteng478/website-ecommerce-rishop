<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Kategori;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function index(){

		// mengambil data dari table pegawai
    	$nama_kategori = Kategori::all();
        return view('FormAdmin/kategoriproduk', compact('nama_kategori'));
				
		}

	public function tambah(){

		return view('FormAdmin/MenuKategoriProduk/tambah');
	}

   
	public function store(Request $request){	
	// insert data ke table pegawai
  $nama_kategori = new Kategori;
  $nama_kategori->nama_kategori = $request->nama_kategori;
  $nama_kategori->save();


  return redirect('/kategoriproduk');

	}

	public function hapus($id)
		{
	// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('kategori_produk')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman pegawai
	return redirect('/kategoriproduk');
}
}