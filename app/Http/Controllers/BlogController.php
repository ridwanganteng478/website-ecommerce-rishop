<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;

class BlogController extends Controller
{
	 public function index(){
	
     $data['banner'] = \DB::table('gambar')->get();
     $data['berita'] = \DB::table('berita')->get();
     return view('FormUser/blog', $data);
	 }
	 public function content(){
	
	 $data['banner'] = \DB::table('gambar')->get();
	 $data['berita'] = \DB::table('berita')->get();
	 return view ('FormUser/blog',$data);
	 }
	 public function readmore($id){
	 $data['banner'] = \DB::table('gambar')->get();
	 $berita['berita'] = \DB::table('berita')->where('id',$id)->get();
	  $berita['beritas'] = \DB::table('berita')->get();
	 return view ('FormUser/MenuBlog/isireadmore',$berita,$data);
	 }
}
