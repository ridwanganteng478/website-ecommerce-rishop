<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Cart;
use App\Produk;

class CartController extends Controller
{
    public function cart($id){

    	$produk = Produk::find($id);
    	Cart::create([
    		'nama_user' => auth()->user()->name,
    		'nama_produk' => $produk->nama_produk,
    		'gambar_produk' => $produk->file,
    		'harga_produk' => $produk->harga,
    		'stok' => 1,
    	]);

    	return redirect()->back();
    }

    public function hapus($id)
        {
    // menghapus data pegawai berdasarkan id yang dipilih
    DB::table('cart')->where('id',$id)->delete();
        
    // alihkan halaman ke halaman pegawai
    return redirect('/cart');
}
}
