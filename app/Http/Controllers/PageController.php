<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\City;
use App\province;
use App\Produk;
use App\Gambar;
use App\Cart;
use App\Bayar;

class PageController extends Controller
{
    public function index(){
    	$data2['banner'] = \DB::table('gambar')->get();
    	$data['city'] = \DB::table('city')->get();
    	$data['province'] = \DB::table('province')->get();
    	$data['produk'] = \DB::table('cart')->where('nama_user', auth()->user()->name)->get();
        $data['subtotal'] = Cart::where('nama_user',auth()->user()->name)->sum('harga_produk');
    	return view('FormUser/checkout',$data,$data2);
    }
 public function indexTo(){
        $data2['banner'] = \DB::table('gambar')->get();
        $data['city'] = \DB::table('city')->get();
        $data['province'] = \DB::table('province')->get();
        $data['produk'] = \DB::table('cart')->where('nama_user', auth()->user()->name)->get();
        $data['subtotal'] = Cart::where('nama_user',auth()->user()->name)->sum('harga_produk');
        return view('FormUser/MenuCheckout/contentongkir',$data,$data2);
    }


    public function getprovince(){

    	$clients = new Client();


    	try {
    		$response = $clients->get('https://api.rajaongkir.com/starter/province',
    			array(

    				'headers' => array(
    					'key' => 'da6be83ceabb6a2b00fc22abdaa156e7',
    					)

    				)
    			);

    	}catch(RequestException $e){
    		var_dump($e->getResponse()->getBody()->getContents());
    	}

    	$json = $response ->getBody()->getContents();

    	$array_result = json_decode($json,true);

    	//print_r($array_result);

    	//echo  $array_result["rajaongkir"]["results"][1]["province"];

    	for ($i = 0; $i < count ($array_result["rajaongkir"]["results"]);$i++)
    	{
    		$province = new \App\Province;
    		$province->id = $array_result["rajaongkir"]["results"][$i]["province_id"];
    		$province->name = $array_result["rajaongkir"]["results"][$i]["province"];
    		$province->save();
    	}

    }

    public function getcity(){

    	$clients = new Client();


    	try {
    		$response = $clients->get('https://api.rajaongkir.com/starter/city',
    			array(

    				'headers' => array(
    					'key' => 'da6be83ceabb6a2b00fc22abdaa156e7',
    					)

    				)
    			);

    	}catch(RequestException $e){
    		var_dump($e->getResponse()->getBody()->getContents());
    	}

    	$json = $response ->getBody()->getContents();

    	$array_result = json_decode($json,true);

    	for ($i = 0; $i<count($array_result["rajaongkir"]["results"]);$i++)
    	{
    		$city = new \App\City;
    		$city->id = $array_result["rajaongkir"]["results"][$i]["city_id"];
    		$city->name = $array_result["rajaongkir"]["results"][$i]["city_name"];
    		$city->id_province = $array_result["rajaongkir"]["results"][$i]["province_id"];
    		$city->save();
    	}

    }

    public function checkshipping(){
    	$title = "Check Shipping";
    	$data['city'] = DB::table('city')->get();


    	return view('FormUser/MenuCheckout/contentongkir',compact('title','data'));
    	

    }
    public function processShipping(Request $request){
    	$title = "Check Shipping Results";
    		$clients = new Client();



    	try {
    		$response = $clients->request('POST','https://api.rajaongkir.com/starter/cost',
    			
    			[
    			'body' =>'origin=22&destination'.$request->destination.'&weight'.$request->weight.
                '&courier='.$request->courier,
    					'headers' => [
    							'key' => 'da6be83ceabb6a2b00fc22abdaa156e7',
    							'content-type' => 'application/x-www-form-urlencoded',
    					]
    			]

    			);

    	}catch(RequestException $e){
    		var_dump($e->getResponse()->getBody()->getContents());
    	}

    	$json = $response ->getBody()->getContents();

    	$array_result = json_decode($json,true);

    	$origin =  $array_result["rajaongkir"]["origin_details"]["city_name"];

    	$destination = $array_result["rajaongkir"]["destination_details"]["city_name"];

    	//print_r($array_result);
    	//echo  $array_result["rajaongkir"]["results"][0]["costs"][1]["cost"][0]["value"];

    	$data=array(
            'title'=>$title,'origin'=>$origin,'destination'=>$destination,'array_result'=>$array_result
            );
        return view('FormUser/MenuCheckout/contentongkir',$data);
    }

    public function bayar(Request $request){
        $cart = Cart::where('nama_user', auth()->user()->name)->get();
        $total = Cart::where('nama_user', auth()->user()->name)->sum('harga_produk');
        foreach ($cart as $cr) {
            Bayar::create([
                'city' => $request->city,
                'province' => $request->province,
                'no_hp' => $request->no_hp,
                'nama_produk' => $cr->nama_produk,
                'harga' => $cr->harga_produk,
                'ongkir' => 0,
                'total' => $total,
            ]);
        }
        return redirect('/show');
    }

    public function show(){
        $data2['banner'] = \DB::table('gambar')->get();
        $data['city'] = \DB::table('city')->get();
        $data['province'] = \DB::table('province')->get();
        $data['bayar'] = \DB::table('bayar')->get();
        $data['produk'] = \DB::table('cart')->where('nama_user', auth()->user()->name)->get();
        $data['subtotal'] = Cart::where('nama_user',auth()->user()->name)->sum('harga_produk');
        
        return view('FormUser/MenuCheckout/show',$data,$data2);
    }

}
